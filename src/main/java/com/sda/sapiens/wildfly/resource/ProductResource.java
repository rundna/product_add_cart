package com.sda.sapiens.wildfly.resource;

import com.sda.sapiens.wildfly.model.dto.ProductDto;
import com.sda.sapiens.wildfly.service.ProductService;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import jakarta.ejb.Stateless;
import jakarta.inject.Inject;
import jakarta.ws.rs.*;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import lombok.extern.java.Log;


@Log
@Stateless
@Path("/product")
public class ProductResource {

    @Inject
    private ProductService productService;

/*    @Inject
    public ProductResource(ProductService productService) {
        this.productService = productService;
    }*/

    @GET
    @Path("")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findAll() {
        log.info("Request to get all prod.");
        return Response
                .status(Response.Status.OK)
                .entity(productService.findAll())
                .build();


    }

    @GET
    @Path("/{phrase}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response findByName(@PathParam(value = "phrase") String searchedPhrase) {
        log.info("Request to get all prod. by name" + searchedPhrase);
        return Response
                .status(Response.Status.OK)
                .entity(productService.findProductsByName(searchedPhrase))
                .build();


    }

    @POST
    @Path("/")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response addProduct(ProductDto newProductInformation) {
        log.info("Request to add " + newProductInformation);
        return Response
                .status(Response.Status.OK)
                .entity(productService.addProduct(newProductInformation))
                .build();
    }
}
