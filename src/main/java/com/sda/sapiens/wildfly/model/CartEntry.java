package com.sda.sapiens.wildfly.model;

import jakarta.persistence.*;
import lombok.*;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
public class CartEntry {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @ManyToOne
    private Product product;
    private double amount;
    @Enumerated(EnumType.STRING)
    private MeasureUnit unit;

    @ManyToOne()
    @ToString.Exclude
    @EqualsAndHashCode.Exclude
    private Cart cart;

}
