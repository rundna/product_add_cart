package com.sda.sapiens.wildfly.exception;

import com.sda.sapiens.wildfly.model.dto.ErrorResponse;
import jakarta.ejb.EJBException;
import jakarta.ejb.Stateless;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;
import jakarta.ws.rs.core.Response;
import jakarta.ws.rs.ext.ExceptionMapper;
import jakarta.ws.rs.ext.Provider;

import java.time.LocalDateTime;
@Provider
@Stateless
public class ResourceExceptionHandler implements ExceptionMapper<EJBException> {

    @Override
    //@Produces(MediaType.APPLICATION_JSON)
    public Response toResponse(EJBException exception) {
        return Response.status(Response.Status.FOUND)
                .entity(new ErrorResponse(
                        LocalDateTime.now(),
                        exception.getMessage(),
                        exception.getClass().getName()))
                .build();


    }
}
