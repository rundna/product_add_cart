package com.sda.sapiens.wildfly.exception;

import jakarta.ejb.EJBException;

public class ProductAlreadyExistException extends EJBException{
    public ProductAlreadyExistException(String message){
        super(message);
    }
}
